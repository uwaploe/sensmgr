// Agent package provides an interface to a serial sensor and its power
// switch.
package agent

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"log/slog"

	"bitbucket.org/uwaploe/dpipc"
	"bitbucket.org/uwaploe/dpsensors"
	"bitbucket.org/uwaploe/sensmgr/internal/power"
)

type Agent struct {
	sens      dpsensors.DpSensor
	sw        power.Switch
	warmup    time.Duration
	buflen    int
	startcmds []string
	cmdMode   bool
}

type Config struct {
	Name   string `yaml:"name"`
	Class  string `yaml:"class"`
	Warmup int    `yaml:"warmup"`
	Buflen int    `yaml:"buflen"`
}

func New(rw io.ReadWriter, sw power.Switch, cfg Config) (*Agent, error) {
	switch strings.ToLower(cfg.Class) {
	case "sbe52mp":
		return &Agent{
			sens:    dpsensors.NewCtd(cfg.Name, rw),
			sw:      sw,
			warmup:  time.Duration(cfg.Warmup) * time.Millisecond,
			buflen:  cfg.Buflen,
			cmdMode: true,
			startcmds: []string{
				"",
				"outputctdo=y",
				"outputctdoraw=n",
				"outputsn=n",
				"overwritemem=y"},
		}, nil
	case "optode":
		return &Agent{
			sens:   dpsensors.NewOptode(cfg.Name, rw),
			sw:     sw,
			warmup: time.Duration(cfg.Warmup) * time.Millisecond,
			buflen: cfg.Buflen,
		}, nil
	case "acm":
		return &Agent{
			sens:   dpsensors.NewAcm(cfg.Name, rw),
			sw:     sw,
			warmup: time.Duration(cfg.Warmup) * time.Millisecond,
			buflen: cfg.Buflen,
		}, nil
	case "flntu":
		return &Agent{
			sens:   dpsensors.NewFlntu(cfg.Name, rw),
			sw:     sw,
			warmup: time.Duration(cfg.Warmup) * time.Millisecond,
			buflen: cfg.Buflen,
		}, nil
	case "flcd":
		return &Agent{
			sens:   dpsensors.NewFlcd(cfg.Name, rw),
			sw:     sw,
			warmup: time.Duration(cfg.Warmup) * time.Millisecond,
			buflen: cfg.Buflen,
		}, nil
	}

	return nil, fmt.Errorf("Unknown sensor class: %v", cfg.Class)
}

func (a *Agent) Setup(lg *slog.Logger) error {
	if is_on, _ := a.sw.Test(); is_on {
		if a.cmdMode {
			a.sw.Off() //nolint:errcheck
			time.Sleep(time.Second)
		}
	}
	a.sw.On() //nolint:errcheck
	time.Sleep(a.warmup)

	for _, cmd := range a.startcmds {
		if lg != nil {
			lg.Info("command sent",
				slog.String("sensor", a.sens.Name()),
				slog.String("text", cmd))
		}
		resp, err := a.sens.Exec(cmd)
		if err != nil {
			return fmt.Errorf("sending %q: %w", cmd, err)
		}
		if lg != nil {
			lg.Info("response",
				slog.String("sensor", a.sens.Name()),
				slog.String("text", resp))
		}
	}

	if a.cmdMode {
		return a.sens.Start()
	}

	return nil
}

func (a *Agent) Teardown() error {
	var err error
	if a.cmdMode {
		err = a.sens.Stop()
	}
	a.sw.Off() //nolint:errcheck
	return err
}

func (a *Agent) Sample(ctx context.Context) <-chan dpipc.DataRecord {
	ch := make(chan dpipc.DataRecord, a.buflen)
	go func() {
		defer close(ch)

		for raw := range a.sens.Stream(ctx) {
			data, err := a.sens.ParseData(raw)
			if err != nil {
				continue
			}
			ch <- dpipc.DataRecord{Src: a.sens.Name(), T: time.Now(), Data: data}
		}
	}()

	return ch
}

func (a *Agent) SampleBlocking(ctx context.Context, ch chan dpipc.DataRecord) error {
	for raw := range a.sens.Stream(ctx) {
		data, err := a.sens.ParseData(raw)
		if err != nil {
			continue
		}
		ch <- dpipc.DataRecord{Src: a.sens.Name(), T: time.Now(), Data: data}
	}

	return ctx.Err()
}
