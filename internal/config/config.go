// Config manages the sensor configuration
package config

import (
	"errors"
	"fmt"
	"io"
	"os"

	"bitbucket.org/uwaploe/sensmgr/internal/agent"
	yaml "gopkg.in/yaml.v2"
)

var defaultCfg = ProgCfg{
	Sensors: []SensorCfg{
		{
			Config: agent.Config{Name: "ctd_1", Class: "sbe52mp", Warmup: 1000, Buflen: 4},
			Desc:   "Seabird SBE-52 CTD",
			Port:   PortCfg{Device: "/dev/ttyS0", Baud: 9600, Switch: "8160_LCD_D0"},
		},
		{
			Config: agent.Config{Name: "optode_1", Class: "optode", Warmup: 500, Buflen: 20},
			Desc:   "Aanderaa Optode",
			Port:   PortCfg{Device: "/dev/ttyS3", Baud: 9600, Switch: "8160_LCD_D3"},
		},
		{
			Config: agent.Config{Name: "flntu_1", Class: "flntu", Warmup: 500, Buflen: 8},
			Desc:   "Wetlabs FLNTU",
			Port:   PortCfg{Device: "/dev/ttyS5", Baud: 19200, Switch: "8160_LCD_D6", Timeout: 3000},
		},
		{
			Config: agent.Config{Name: "flcd_1", Class: "flcd", Warmup: 500, Buflen: 8},
			Desc:   "Wetlabs FLCD",
			Port:   PortCfg{Device: "/dev/ttyS4", Baud: 19200, Switch: "8160_LCD_D6", Timeout: 3000},
		},
		{
			Config: agent.Config{Name: "acm_1", Class: "acm", Warmup: 500, Buflen: 4},
			Desc:   "FSI Acoustic Current Meter",
			Port:   PortCfg{Device: "/dev/ttyS1", Baud: 19200, Switch: "8160_LCD_D1"},
		},
	},
}

type PortCfg struct {
	Device  string `yaml:"device"`
	Baud    int    `yaml:"baud"`
	Switch  string `yaml:"switch"`
	Timeout int    `yaml:"timeout"`
}

type SensorCfg struct {
	agent.Config `yaml:",inline"`
	Desc         string  `yaml:"desc"`
	Port         PortCfg `yaml:"port"`
}

var ErrSensor = errors.New("Sensor not found")

type ProgCfg struct {
	Sensors []SensorCfg `yaml:"sensors"`
}

func InitConfig(path string) (ProgCfg, error) {
	cfg := defaultCfg
	if path != "" {
		contents, err := os.ReadFile(path)
		if err != nil {
			return cfg, err
		}
		err = cfg.Load(contents)
		if err != nil {
			return cfg, err
		}
	}
	return cfg, nil
}

func (p ProgCfg) FindSensor(name string) (SensorCfg, error) {
	for _, s := range p.Sensors {
		if s.Name == name {
			return s, nil
		}
	}

	return SensorCfg{}, fmt.Errorf("%q: %w", name, ErrSensor)
}

func (c *ProgCfg) Load(contents []byte) error {
	return yaml.Unmarshal(contents, c)
}

func (c ProgCfg) Dump(w io.Writer) error {
	enc := yaml.NewEncoder(w)
	enc.Encode(c) //nolint:errcheck
	return enc.Close()
}
