package config

import "testing"

func TestDefConfig(t *testing.T) {
	cfg, err := InitConfig("")
	if err != nil {
		t.Fatal(err)
	}

	if len(cfg.Sensors) != 5 {
		t.Errorf("Bad sensor count; expected 5, got %d", len(cfg.Sensors))
	}

	names := []string{"ctd_1", "acm_1", "optode_1", "flntu_1", "flcd_1"}
	for _, name := range names {
		_, err := cfg.FindSensor(name)
		if err != nil {
			t.Error(err)
		}
	}
}
