module bitbucket.org/uwaploe/sensmgr

go 1.21.5

require (
	bitbucket.org/mfkenney/go-redisq/v3 v3.2.0
	bitbucket.org/uwaploe/dpipc v0.1.0
	bitbucket.org/uwaploe/dpsensors v0.3.0
	bitbucket.org/uwaploe/tsfpga v0.7.1
	github.com/charmbracelet/bubbletea v0.23.2
	github.com/charmbracelet/lipgloss v0.7.1
	github.com/gomodule/redigo v1.8.9
	go.bug.st/serial v1.6.1
	google.golang.org/grpc v1.40.0
	gopkg.in/yaml.v2 v2.2.3
)

require (
	bitbucket.org/mfkenney/aanderaa v0.2.1 // indirect
	bitbucket.org/mfkenney/fsi v0.1.1 // indirect
	bitbucket.org/mfkenney/seabird v0.2.1 // indirect
	bitbucket.org/mfkenney/wetlabs v0.2.2 // indirect
	github.com/aymanbagabas/go-osc52/v2 v2.0.1 // indirect
	github.com/containerd/console v1.0.3 // indirect
	github.com/creack/goselect v0.1.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-localereader v0.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/muesli/ansi v0.0.0-20211018074035-2e021307bc4b // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.15.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/vmihailenco/msgpack/v5 v5.4.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
