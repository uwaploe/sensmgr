# Deep Profiler Sensor Manager

This repository contains a the serial sensor manager for the DPC along with a couple of sensor testing programs. See the [Wiki](https://bitbucket.org/uwaploe/sensmgr/wiki/Home) for documentation.

## Installation

Download the most recent compressed TAR archive file from the [downloads](https://bitbucket.org/uwaploe/sensmgr/downloads/)section of this repository, copy it to `/home/rsn/stow` on the DPC, and install using the commands shown below (replacing `$VERS` with the current version).

``` shellsession
$ cd ~/stow
$ tar xvzf sensmgr_$VERS_linux_arm5.tar.gz
$ cd sensmgr_$VERS_linux_arm5
$ ./scripts/install.sh
```
