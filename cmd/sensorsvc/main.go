// Sensorsvc streams data from all of the Deep Profiler sensors and writes
// the data records to a Redis queue that is read by the data archiver.
package main

import (
	"context"
	"encoding/binary"
	"flag"
	"fmt"
	"math"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"

	"log/slog"

	redisq "bitbucket.org/mfkenney/go-redisq/v3"
	"bitbucket.org/uwaploe/dpipc"
	"bitbucket.org/uwaploe/sensmgr/internal/agent"
	"bitbucket.org/uwaploe/sensmgr/internal/config"
	"bitbucket.org/uwaploe/sensmgr/internal/power"
	"github.com/gomodule/redigo/redis"
	"go.bug.st/serial"
	"google.golang.org/grpc"
)

const Usage = `Usage: sensorsvc [options]

Stream data from all DP sensors and write the records to a Redis
queue which is read by the data archiver. Exits on SIGTERM, SIGINT,
or SIGHUP.

`

var Version = "dev"
var BuildDate = "unknown"

const prChan = "sbe.slp"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rpcAddr    string        = "localhost:10101"
	rdAddr     string        = "localhost:6379"
	dqName     string        = "archive:queue"
	exList     string        = "sensors:exclude"
	qLen       uint          = 200
	rdTimeout  time.Duration = time.Second * 5
	cfgFile    string
	dumpCfg    bool
	noPressure bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr, "host:port for power control gRPC server")
	flag.StringVar(&rdAddr, "rd-addr", rdAddr, "host:port for Redis server")
	flag.StringVar(&dqName, "queue", dqName, "name of Redis list for the data queue")
	flag.StringVar(&exList, "exlist", exList,
		"name of Redis list of excluded sensors")
	flag.UintVar(&qLen, "qlen", qLen, "maximum data queue length")
	flag.StringVar(&cfgFile, "cfg", cfgFile, "configuration file name")
	flag.BoolVar(&dumpCfg, "dumpcfg", dumpCfg,
		"dump default configuration to stdout and exit")
	flag.DurationVar(&rdTimeout, "timeout", rdTimeout, "serial port read timeout")
	flag.BoolVar(&noPressure, "no-pr", noPressure,
		"do not publish CTD pressure values to Redis")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func openSerialPort(cfg config.PortCfg) (serial.Port, error) {
	mode := &serial.Mode{
		BaudRate: cfg.Baud,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	port, err := serial.Open(cfg.Device, mode)
	if err != nil {
		return port, fmt.Errorf("open %s: %w", cfg.Device, err)
	}

	if cfg.Timeout > 0 {
		_ = port.SetReadTimeout(time.Duration(cfg.Timeout) * time.Millisecond)
	}

	return port, nil
}

func setupSensor(cfg config.SensorCfg) (*agent.Agent, error) {
	// Power switch
	var sw power.Switch

	if cfg.Port.Switch == "" {
		sw = power.NewDummySwitch()
	} else {
		gconn, err := grpcConnect("localhost:10101")
		if err != nil {
			return nil, err
		}
		sw = power.NewDio(gconn, cfg.Port.Switch)
	}

	port, err := openSerialPort(cfg.Port)
	if err != nil {
		return nil, err
	}

	return agent.New(port, sw, cfg.Config)
}

// Check the Redis DB for a list of excluded sensors
func getExcluded(conn redis.Conn, key string) map[string]bool {
	m := make(map[string]bool)
	names, err := redis.Strings(conn.Do("LRANGE", key, 0, -1))
	if err != nil {
		return m
	}
	for _, name := range names {
		m[name] = true
	}

	return m
}

func main() {
	_ = parseCmdLine()
	if dumpCfg {
		cfg, _ := config.InitConfig("")
		cfg.Dump(os.Stdout) //nolint:errcheck
		os.Exit(0)
	}

	slog.SetDefault(initLogger())

	syscfg, err := config.InitConfig(cfgFile)
	if err != nil {
		abort("load configuration", err, slog.String("file", cfgFile))
	}

	conn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		abort("TCP connect failed", err, slog.String("addr", rdAddr))
	}
	defer conn.Close()

	dataq := redisq.NewQueue[dpipc.DataRecord](conn, dqName, qLen)
	if err != nil {
		abort("Cannot open data queue", err, slog.String("name", dqName))
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	var wg sync.WaitGroup
	ch := make(chan dpipc.DataRecord, len(syscfg.Sensors))

	ex := getExcluded(conn, exList)
	for _, cfg := range syscfg.Sensors {
		if ex[cfg.Name] {
			slog.Info("exclude sensor", slog.String("name", cfg.Name))
			continue
		}

		ag, err := setupSensor(cfg)
		if err != nil {
			slog.Error("cannot access sensor", err,
				slog.String("name", cfg.Name))
			continue
		}

		slog.Info("sensor power-on", slog.String("name", cfg.Name))
		err = ag.Setup(slog.Default())
		if err != nil {
			ag.Teardown() //nolint:errcheck
			slog.Error("sensor setup failed", err,
				slog.String("name", cfg.Name))
			continue
		}

		// Start a goroutine to sample this sensor and send each
		// data record to the fan-in channel.
		wg.Add(1)
		go func(a *agent.Agent, name string) {
			defer wg.Done()
			slog.Info("sampling start", slog.String("name", name))
			for rec := range a.Sample(ctx) {
				select {
				case ch <- rec:
				default:
				}
			}
			a.Teardown() //nolint:errcheck
			slog.Info("sensor power-off", slog.String("name", name))
		}(ag, cfg.Name)
	}

	slpbuf := make([]byte, 4)
	for {
		select {
		case rec := <-ch:
			if !noPressure && rec.Src == "ctd_1" {
				// Broadcast the current CTD pressure value
				if m, ok := rec.Data.(map[string]interface{}); ok {
					if pr, ok := m["preswat"].(float32); ok {
						binary.LittleEndian.PutUint32(slpbuf, math.Float32bits(pr))
						conn.Do("PUBLISH", prChan, slpbuf) //nolint:errcheck
					}
				}
			}
			// Append each record to the archiver queue
			err = dataq.Put(rec)
			if err != nil {
				slog.Error("data record queue", err, slog.String("sensor", rec.Src))
			}
		case s := <-sigs:
			slog.Info("interrupt", slog.String("signal", s.String()))
			cancel()
			slog.Info("Waiting for threads to exit...")
			wg.Wait()
			slog.Info("Done")
			return
		}
	}

}
