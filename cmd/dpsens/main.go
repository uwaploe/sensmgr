//
package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"runtime"
	"time"

	"log/slog"

	"bitbucket.org/uwaploe/sensmgr/internal/agent"
	"bitbucket.org/uwaploe/sensmgr/internal/config"
	"bitbucket.org/uwaploe/sensmgr/internal/power"
	tea "github.com/charmbracelet/bubbletea"
	"go.bug.st/serial"
	"google.golang.org/grpc"
)

const Usage = `Usage: dpsens [options]

Text UI for DP sensor testing.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rpcAddr   string        = "localhost:10101"
	rdTimeout time.Duration = time.Second * 5
	cfgFile   string
	dumpCfg   bool
	traceMode bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr, "host:port for power control gRPC server")
	flag.StringVar(&cfgFile, "cfg", cfgFile, "configuration file name")
	flag.BoolVar(&dumpCfg, "dumpcfg", dumpCfg,
		"dump default configuration to stdout and exit")
	flag.BoolVar(&traceMode, "trace", traceMode,
		"log sensor output to trace.txt in current directory")
	flag.DurationVar(&rdTimeout, "timeout", rdTimeout, "serial port read timeout")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func openSerialPort(cfg config.PortCfg) (serial.Port, error) {
	mode := &serial.Mode{
		BaudRate: cfg.Baud,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	port, err := serial.Open(cfg.Device, mode)
	if err != nil {
		return port, fmt.Errorf("open %s: %w", cfg.Device, err)
	}

	if cfg.Timeout > 0 {
		_ = port.SetReadTimeout(time.Duration(cfg.Timeout) * time.Millisecond)
	}

	return port, nil
}

func setupSensor(cfg config.SensorCfg) (*agent.Agent, error) {
	// Power switch
	var sw power.Switch

	if cfg.Port.Switch == "" {
		sw = power.NewDummySwitch()
	} else {
		gconn, err := grpcConnect("localhost:10101")
		if err != nil {
			return nil, err
		}
		sw = power.NewDio(gconn, cfg.Port.Switch)
	}

	port, err := openSerialPort(cfg.Port)
	if err != nil {
		return nil, err
	}

	return agent.New(port, sw, cfg.Config)
}

func main() {
	_ = parseCmdLine()
	if dumpCfg {
		cfg, _ := config.InitConfig("")
		cfg.Dump(os.Stdout) //nolint:errcheck
		os.Exit(0)
	}

	slog.SetDefault(initLogger())

	syscfg, err := config.InitConfig(cfgFile)
	if err != nil {
		abort("load configuration", err, slog.String("file", cfgFile))
	}

	var w io.Writer

	if traceMode {
		f, err := os.Create("trace.txt")
		if err != nil {
			abort("open trace file", err)
		}
		defer f.Close()
		w = f
	}

	p := tea.NewProgram(initialModel(syscfg, w), tea.WithAltScreen())
	if _, err := p.Run(); err != nil {
		slog.Error("", err)
	}
}

// Local Variables:
// compile-command: "GOOS=linux GOARCH=arm GOARM=5 go build -ldflags '-s -w'"
// End:
