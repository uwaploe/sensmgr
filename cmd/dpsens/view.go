package main

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"bitbucket.org/uwaploe/dpipc"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type UiView struct {
	Name       string
	Action     func(Model) (Model, tea.Cmd)
	ItemLength int
	Render     func(Model) string
	Footer     string
}

func (m Model) View() string {
	header := m.labelStyle.Render("DP Sensor Test") + "\n\n"
	info := fmt.Sprintf("Selected sensor: %s\n\n", m.labelStyle.Render(m.Selected.Name))
	view := m.Screen.Render(m)
	footer := "\n\n" + m.textStyle.Render(m.Screen.Footer)
	return header + info + view + footer
}

func (m Model) viewData(rec dpipc.DataRecord) string {
	var b strings.Builder

	data, ok := rec.Data.(map[string]interface{})
	if !ok {
		return m.dataStyle.Render(rec.String())
	}

	keys := make([]string, 0, len(data))
	for k := range data {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	b.WriteString(m.renderValue("Time", rec.T.Format(time.RFC3339)) + "\n")
	for _, k := range keys {
		b.WriteString(m.renderValue(k, fmt.Sprint(data[k])) + "\n")
	}
	return b.String()
}

var SensorListScreen = UiView{
	Name:   "SensorList",
	Footer: "(↑: up • ↓: down • Enter: select • ctrl+c: quit)",
	Render: func(m Model) string {
		var (
			list  string
			style lipgloss.Style
		)
		for i, sens := range m.cfg.Sensors {
			cursor := " "
			if m.Cursor == i {
				cursor = ">"
				style = m.labelStyle
			} else {
				style = m.textStyle
			}
			list += fmt.Sprintf("%s %s\n", cursor, style.Render(sens.Desc))
		}
		return list
	},
	Action: func(m Model) (Model, tea.Cmd) {
		m.Selected = m.cfg.Sensors[m.Cursor]
		m.Screen = SensorMonScreen
		m.Cursor = 0
		m.a, m.err = setupSensor(m.Selected)
		if m.err != nil {
			m.Screen = SensorErrScreen
		}
		m.ctx, m.cancel = context.WithCancel(context.Background())
		m.err = m.a.Setup(nil)
		if m.err != nil {
			m.Screen = SensorErrScreen
			return m, nil
		}
		return m, tea.Batch(listenForData(m.ctx, m.a, m.ch),
			waitForData(m.ch))
	},
}

var SensorErrScreen = UiView{
	Name:   "SensorError",
	Footer: "(esc: change sensor • ctrl+c: quit)",
	Render: func(m Model) string {
		return fmt.Sprintf("ERROR: %v\n", m.err)
	},
	Action: func(m Model) (Model, tea.Cmd) {
		return m, nil
	},
}

var SensorMonScreen = UiView{
	Name:   "SensorMonitor",
	Footer: "(esc: change sensor • ctrl+c: quit)",
	Render: func(m Model) string {
		if m.rec.Src == "" {
			return m.emphStyle.Render("** Waiting for data... ") + "\n"
		}
		return m.viewData(m.rec)
	},
	Action: func(m Model) (Model, tea.Cmd) {
		return m, nil
	},
}
