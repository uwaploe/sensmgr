package main

import (
	"context"
	"fmt"
	"io"

	"bitbucket.org/uwaploe/dpipc"
	"bitbucket.org/uwaploe/sensmgr/internal/agent"
	"bitbucket.org/uwaploe/sensmgr/internal/config"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type modelStyle struct {
	labelStyle lipgloss.Style
	textStyle  lipgloss.Style
	emphStyle  lipgloss.Style
	dataStyle  lipgloss.Style
}

func (m modelStyle) renderValue(key, val string) string {
	return lipgloss.JoinHorizontal(lipgloss.Top, m.labelStyle.Render(key),
		m.dataStyle.Render(val))
}

type Model struct {
	ch       chan dpipc.DataRecord
	cfg      config.ProgCfg
	ctx      context.Context
	cancel   context.CancelFunc
	a        *agent.Agent
	err      error
	rec      dpipc.DataRecord
	wtr      io.Writer
	Cursor   int
	Selected config.SensorCfg
	Screen   UiView
	modelStyle
}

func initialModel(cfg config.ProgCfg, wtr io.Writer) Model {
	m := Model{
		cfg:    cfg,
		ch:     make(chan dpipc.DataRecord, 1),
		Screen: SensorListScreen,
		wtr:    wtr,
	}
	m.Screen.ItemLength = len(m.cfg.Sensors)
	m.labelStyle = lipgloss.NewStyle().Bold(true)
	m.emphStyle = m.labelStyle.Copy().Foreground(lipgloss.Color("#FF0000"))
	m.textStyle = lipgloss.NewStyle()
	m.dataStyle = m.textStyle.Copy().PaddingLeft(4)
	return m
}

func (m Model) Init() tea.Cmd {
	return nil
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case dpipc.DataRecord:
		m.rec = msg
		if m.wtr != nil {
			fmt.Fprintf(m.wtr, "%s\n", m.rec)
		}
		return m, waitForData(m.ch)
	case tea.WindowSizeMsg:
		m.dataStyle = m.dataStyle.Width(msg.Width)
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			if m.cancel != nil {
				m.cancel()
				m.cancel = nil
			}
			if m.a != nil {
				m.a.Teardown() //nolint:errcheck
				m.a = nil
			}
			return m, tea.Quit
		case "esc", "s":
			m.Screen = SensorListScreen
			m.Screen.ItemLength = len(m.cfg.Sensors)
			if m.cancel != nil {
				m.cancel()
				m.cancel = nil
			}
			if m.a != nil {
				m.a.Teardown() //nolint:errcheck
				m.a = nil
			}
			m.rec = dpipc.DataRecord{}
			m.Selected = config.SensorCfg{}
		case "up", "k":
			if m.Cursor > 0 {
				m.Cursor--
			}
		case "down", "j":
			if m.Cursor < m.Screen.ItemLength-1 {
				m.Cursor++
			}
		case "enter":
			save := m.Cursor
			m, cmd = m.Screen.Action(m)
			m.Cursor = save
		}
	}

	return m, cmd
}

func listenForData(ctx context.Context, a *agent.Agent,
	ch chan dpipc.DataRecord) tea.Cmd {
	return func() tea.Msg {
		return a.SampleBlocking(ctx, ch)
	}
}

func waitForData(ch chan dpipc.DataRecord) tea.Cmd {
	return func() tea.Msg {
		return <-ch
	}
}
