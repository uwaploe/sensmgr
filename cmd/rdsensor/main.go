// Rdsensor streams data from a single Deep Profiler sensor
package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"log/slog"

	"bitbucket.org/uwaploe/sensmgr/internal/agent"
	"bitbucket.org/uwaploe/sensmgr/internal/config"
	"bitbucket.org/uwaploe/sensmgr/internal/power"
	"go.bug.st/serial"
	"google.golang.org/grpc"
)

const Usage = `Usage: rdsensor [options] sensorname

Stream data from a single DP sensor to standard output.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rpcAddr   string        = "localhost:10101"
	rdTimeout time.Duration = time.Second * 5
	cfgFile   string
	dumpCfg   bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr, "host:port for power control gRPC server")
	flag.StringVar(&cfgFile, "cfg", cfgFile, "configuration file name")
	flag.BoolVar(&dumpCfg, "dumpcfg", dumpCfg,
		"dump default configuration to stdout and exit")
	flag.DurationVar(&rdTimeout, "timeout", rdTimeout, "serial port read timeout")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func openSerialPort(cfg config.PortCfg) (serial.Port, error) {
	mode := &serial.Mode{
		BaudRate: cfg.Baud,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	port, err := serial.Open(cfg.Device, mode)
	if err != nil {
		return port, fmt.Errorf("open %s: %w", cfg.Device, err)
	}

	if cfg.Timeout > 0 {
		_ = port.SetReadTimeout(time.Duration(cfg.Timeout) * time.Millisecond)
	}

	return port, nil
}

func setupSensor(cfg config.SensorCfg) (*agent.Agent, error) {
	// Power switch
	var sw power.Switch

	if cfg.Port.Switch == "" {
		sw = power.NewDummySwitch()
	} else {
		gconn, err := grpcConnect("localhost:10101")
		if err != nil {
			return nil, err
		}
		sw = power.NewDio(gconn, cfg.Port.Switch)
	}

	port, err := openSerialPort(cfg.Port)
	if err != nil {
		return nil, err
	}

	return agent.New(port, sw, cfg.Config)
}

func main() {
	args := parseCmdLine()
	if dumpCfg {
		cfg, _ := config.InitConfig("")
		cfg.Dump(os.Stdout) //nolint:errcheck
		os.Exit(0)
	}

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	slog.SetDefault(initLogger())

	syscfg, err := config.InitConfig(cfgFile)
	if err != nil {
		abort("load configuration", err, slog.String("file", cfgFile))
	}

	cfg, err := syscfg.FindSensor(args[0])
	if err != nil {
		abort("", err, slog.String("name", args[0]))
	}

	agent, err := setupSensor(cfg)
	if err != nil {
		abort("cannot access sensor", err, slog.String("name", cfg.Name))
	}

	slog.Info("sensor power-on", slog.String("name", cfg.Name))
	err = agent.Setup(nil)
	if err != nil {
		agent.Teardown() //nolint:errcheck
		abort("sensor setup failed", err,
			slog.String("name", cfg.Name))
	}
	defer agent.Teardown() //nolint:errcheck

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Cancel on signal reception
	go func() {
		s, more := <-sigs
		if more {
			slog.Info("interrupt", slog.String("signal", s.String()))
			cancel()
		}
	}()

	for rec := range agent.Sample(ctx) {
		fmt.Printf("%s\n", rec)
	}

	slog.Info("sensor power-off", slog.String("name", cfg.Name))
}
